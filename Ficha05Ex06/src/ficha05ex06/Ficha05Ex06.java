/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha05ex06;

import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class Ficha05Ex06 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        final double precoTinta = 2.75;
        final double areaCubo = (2 * 2 * 6);
        
        int cubos = 0;
        
        System.out.println("Digite o numero de cubos: ");
        cubos = scanner.nextInt();
        
        System.out.println("Custo para pintar os cubos: " + Math.round(((cubos * areaCubo) / 7.5)) * precoTinta + "€");
    }
    
}
