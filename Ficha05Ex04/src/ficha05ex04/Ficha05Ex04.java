/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha05ex04;

import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class Ficha05Ex04 {
    
    private static final Scanner scanner = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int totalEleitores = 0, totalValidos = 0, totalBrancos = 0, totalNulos = 0;

        System.out.println("Digite o total de eleitores: ");
	totalEleitores = scanner.nextInt();

        System.out.println("Digite o número de votos em branco: ");
	totalBrancos = scanner.nextInt();

        System.out.println("Digite o número de votos nulos: ");
	totalNulos = scanner.nextInt();

        System.out.println("Digite o número de votos válidos: ");
	totalValidos = scanner.nextInt();

        System.out.println("Percentagem de votos em branco: " + (totalBrancos * 100) / totalEleitores + "%");
        System.out.println("Percentagem de votos nulos: " + (totalNulos * 100) / totalEleitores + "%");
        System.out.println("Percentagem de votos válidos: " + (totalValidos * 100) / totalEleitores + "%");
    }
    
}
