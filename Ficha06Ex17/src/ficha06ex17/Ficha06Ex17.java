/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex17;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex17 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double valor, numMoedas;
        
        System.out.println("Introduza um valor em cêntimos: ");
        valor = scanner.nextInt();
        
        numMoedas = Math.floor(valor / 200);
        if (numMoedas > 0) {
            System.out.println(numMoedas + " Moedas de 2 Euros");
            valor = valor - (numMoedas * 200);
        }
        
        numMoedas = Math.floor(valor / 100);
        if (numMoedas > 0) {
            System.out.println(numMoedas + " Moedas de 1 Euros");
            valor = valor - (numMoedas * 100);
        }
        
        numMoedas = Math.floor(valor / 50);
        if (numMoedas > 0) {
            System.out.println(numMoedas + " Moedas de 50 Cêntimos");
            valor = valor - (numMoedas * 50);
        }
        
        numMoedas = Math.floor(valor / 20);
        if (numMoedas > 0) {
            System.out.println(numMoedas + " Moedas de 20 Cêntimos");
            valor = valor - (numMoedas * 20);
        }
        
        numMoedas = Math.floor(valor / 10);
        if (numMoedas > 0) {
            System.out.println(numMoedas + " Moedas de 10 Cêntimos");
            valor = valor - (numMoedas * 10);
        }
        
        numMoedas = Math.floor(valor / 5);
        if (numMoedas > 0) {
            System.out.println(numMoedas + "Moedas de 5 Cêntimos");
            valor = valor - (numMoedas * 5);
        }
        
        numMoedas = Math.floor(valor / 2);
        if (numMoedas > 0) {
            System.out.println(numMoedas + "Moedas de 2 Cêntimos");
            valor = valor - (numMoedas * 2);
        }

        if (valor > 0) {
            System.out.println(valor + " Moedas de 1 Cêntimos");
        }
    }
    
}
