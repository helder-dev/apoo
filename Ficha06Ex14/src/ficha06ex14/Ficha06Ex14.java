/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex14;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex14 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double kWh, diff_kWh = 0.0;
        
        for (int i = 1; i <= 12; i++) {
            System.out.print("Introduza o valor de electricidade gasto no " + i + "º em kWh: ");
            kWh = scanner.nextDouble();
            
            diff_kWh = diff_kWh + (kWh - 138.9);
        }

        if (diff_kWh > 0) {
            System.out.println("Terá de pagar um total de: " + (diff_kWh * 0.18) + "€");
        } else if (diff_kWh < 0) {
            System.out.println("Terá de recever um total de: " + Math.abs(diff_kWh * 0.18) + "€");
        } else {
            System.out.println("Não terá de pagar ou receber nada.");
        }
    }
    
}
