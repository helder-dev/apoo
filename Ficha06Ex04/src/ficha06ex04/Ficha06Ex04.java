/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex04;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex04 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double media;
        
        System.out.println("Introduza a média do aluno: ");
        media = scanner.nextDouble();
        
        if (media >= 5.0) {
            if (media >= 7.0) {
                System.out.println("Aluno aprovado.");
            } else {
                System.out.println("Aluno necessita fazer outra avaliação.");
            }
        } else {
            System.out.println("Aluno reprovado.");
        }
    }
    
}
