/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex02;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex02 {

    private static final Scanner scanner = new Scanner(System.in);
        
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        short operacao;
        double num1, num2;
        
        do {
             System.out.println("Digite a operação desejada realizar:");
             System.out.println("  1. Soma");      
             System.out.println("  2. Subtração");      
             System.out.println("  3. Multiplicação");      
             System.out.println("  4. Divisão");      
             System.out.println("  0. Sair");
             
             System.out.print("Operação: ");
             operacao = scanner.nextShort();
             
             if (operacao == 0) {
            	 break;
             }
             
             if (!validarOperacao(operacao)) {
            	 continue;
             }
             
             System.out.print("Introduza o primeiro valor: ");
             num1 = scanner.nextDouble();
             System.out.print("Digite o segundo valor: ");
             num2 = scanner.nextDouble();
             
             if (!validarDados(operacao, num1, num2)) {
            	 continue;
             }

             System.out.println("RESULTADO: " + calcular(operacao, num1, num2) + "\n");
             break;
        } while (operacao != 0);
    }
    
    static boolean validarOperacao (short operacao) {
       	boolean retValue = true;
        
    	if (operacao > 4) {
            System.out.println("ERRO: A operação escolhida não é válida.\n");
            retValue = false;
       	}
        
    	return retValue;
    }
    
    static boolean validarDados (short operacao, double num1, double num2) {
    	boolean retValue = true; 
        
    	if (operacao == 4 && (num1 == 0 || num2 == 0)) {
            System.out.println("ERRO: Não é possível dividir por 0.\n");
            retValue = false;
        }
        
    	return retValue;
    }
    
    static double calcular (short operacao, double num1, double num2) {
    	double retValue = 0;
        
    	switch (operacao) {
            case 1:
                retValue = num1 + num2;
                break;
            case 2: 
                retValue = num1 - num2;
                break;
            case 3: 
                retValue = num1 * num2;
                break;
            case 4:
                retValue = num1 / num2;
                break;
    	}
        
    	return retValue;
    }
}
