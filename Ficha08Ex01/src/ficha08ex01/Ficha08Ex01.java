/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha08ex01;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha08Ex01 {

    private static final Scanner scanner = new Scanner(System.in);
  
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int n;
        
        System.out.println("Introduza o tamanho do vector: ");
        n = scanner.nextInt();
        
        int vec[] = new int[n];
        
        for (int i = 0; i < n; i++) {
            System.out.print("Introduza o valor para a " + (i + 1) + "ª posição do vector: ");
            vec[i] = scanner.nextInt();
        }
        
        System.out.println("Ordem inversa:" + inverterVector(vec, n));
        System.out.println("Média ímpares: " + getMediaNumImpares(vec, n));
    }
    
    /**
     * Função para inverter o vector.
     * @param vec
     * @param n
     * @return 
     */
    public static String inverterVector(int vec[], int n) {
        String retValue = "";
        
        for (int i = (n - 1); i >= 0; i--) {
            retValue = retValue + " " + vec[i];
        }
        
        return retValue;
    }
    
    /**
     * Função para devolver média dos número ímpares.
     * @param vec
     * @param n
     * @return 
     */
    public static double getMediaNumImpares(int vec[], int n) {
        double retValue;
        int soma = 0, count = 0;
        
        for (int i = 0; i < n; i++) {
            if ((vec[i] % 2) != 0) {
                soma = soma + vec[i];
                count++;
            }
        }
        
        retValue = (soma / count);
        
        return retValue;
    }

}
