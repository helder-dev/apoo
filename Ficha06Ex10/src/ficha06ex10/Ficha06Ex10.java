/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex10;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex10 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int numChamadas, totalMinutos = 0;
        double valorTotal = 0.0;
        
        System.out.println("Introduza o número de chamada efectuadas: ");
        numChamadas = scanner.nextInt();
        
        for(int i = 0; i < numChamadas; i++) {
            System.out.print("Introduza o valor da " + (i + 1) + "ª chamada: ");
            valorTotal = valorTotal + scanner.nextDouble();
            
            System.out.print("Introduza o duração da " + (i + 1) + "ª chamada: ");
            totalMinutos = totalMinutos + scanner.nextInt();
        }
        
        System.out.println("O valor total das chamadas é de " + valorTotal + "€");
        System.out.println("A duração total das chamadas é de " + totalMinutos + " minutos");
    }
    
}
