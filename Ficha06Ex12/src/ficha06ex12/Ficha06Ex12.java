/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex12;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex12 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int totalRefeicoes = 0;
        double valorTotal = 0.0;
        
        for(int i = 0; i < 7; i++) {
            System.out.print("Introduza o número de refeições servidas no " + (i + 1) + "º dia: ");
            totalRefeicoes = totalRefeicoes + scanner.nextInt();
            
            System.out.print("Introduza o valor total das refeições servidas no " + (i + 1) + "º dia: ");
            valorTotal = valorTotal + scanner.nextDouble();
        }
        
        System.out.println("O total das refeições servidas na 1ª semana é de " + totalRefeicoes + " refeições.");
        System.out.println("A média do valor gasto nas refeições servidas na 1ª semana é de " + (valorTotal / 7) + " €");
    }
    
}
