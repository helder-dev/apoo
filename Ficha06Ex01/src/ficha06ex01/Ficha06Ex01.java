/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex01;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex01 {

    private static final Scanner scanner = new Scanner(System.in);
    private static final int NUM_TESTES = 3;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String codAluno;
        double[] notas = new double[NUM_TESTES];
        int idxMaxNota = 0, divisor = 0;
        double MP, dividendo = 0.0;
        
        System.out.println("Introduza o código do aluno: ");
        codAluno = scanner.nextLine();
        
        for(int i = 0; i < NUM_TESTES; i++) {
            do { 
                System.out.print("Digite a nota do teste " + (i + 1) + ": ");
                notas[i] = scanner.nextDouble();
            } while (notas[i] < 0.0 || notas[i] > 20.0);
            
            if ((i > 0) && (notas[i] > notas[i - 1])) {
                idxMaxNota = i;
            }
        }
        
        for(int i = 0; i < NUM_TESTES; i++) {
            if (i == idxMaxNota) {
                dividendo = dividendo + (notas[i] * 4);
                divisor = divisor + 4;
            } else {
                dividendo = dividendo + (notas[i] * 3);
                divisor = divisor + 3;
            }
        }
        
        MP = (dividendo / divisor);
        
        System.out.println("Código do Aluno: " + codAluno);
        System.out.println("Média Ponderada: " + MP);
        if (MP >= 10) {
            System.out.println("APROVADO");
        } else {
            System.out.println("REPROVADO");
        }
    }
    
}
