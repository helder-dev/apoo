/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha08ex02;

/**
 *
 * @author Hélder Oliveira
 */
public class Tempo {
    private int horas;
    private int minutos;
    private int segundos;
    
    private static final int HORAS_POR_OMISSAO = 0;
    private static final int MINUTOS_POR_OMISSAO = 0;
    private static final int SEGUNDOS_POR_OMISSAO = 0;
    
    public Tempo() {
        horas = HORAS_POR_OMISSAO;
        minutos = MINUTOS_POR_OMISSAO;
        segundos = SEGUNDOS_POR_OMISSAO;
    }
    
    public Tempo(int horas, int minutos, int segundos) {
        this.horas = horas;
        this.minutos = minutos;
        this.segundos = segundos;
    }
    
    
    public String getHorasFormato24() {
        return horas + ":" + minutos + ":" + segundos;
    }
    
    public String getHorasFormato12() {
        String retValue;
        
        if (horas >= 12) {
            retValue = (horas - 12) + ":" + minutos + ":" + segundos + " PM";
        } else {
            retValue = horas + ":" + minutos + ":" + segundos + " AM";
        }
        
        return retValue;
    }
    
    public void addSegundo() {
        if (segundos < 59) {
            segundos++;
        } else {
            segundos = SEGUNDOS_POR_OMISSAO;
            if (minutos < 59) {
                minutos++;
            } else {
                minutos = MINUTOS_POR_OMISSAO;
                if (horas < 23) {
                    horas++;
                } else {
                    horas = HORAS_POR_OMISSAO;
                }
            }
        }
    }
    
    public boolean isMaior(Tempo outroTempo) {
        int totalSegundos = contarSegundos();
        int totalSegundos1 = outroTempo.contarSegundos();

        return totalSegundos > totalSegundos1;
    }
    
    public boolean isMaior(int horas, int minutos, int segundos) {
        int totalSegundos = contarSegundos();
        Tempo outroTempo = new Tempo(horas, minutos, segundos);
        int totalSegundos1 = outroTempo.contarSegundos();

        return totalSegundos > totalSegundos1;
    }
    
    public int calcularDiferencaSegundos(Tempo outroTempo) {
        int totalSegundos = contarSegundos();
        int totalSegundos1 = outroTempo.contarSegundos();
        
        return Math.abs(totalSegundos - totalSegundos1);
    }
    
    public Tempo calcularDiferencaTempo(Tempo outroTempo) {
        int totalSegundos = contarSegundos();
        int totalSegundos1 = outroTempo.contarSegundos();
        
        int diferenca = Math.abs(totalSegundos - totalSegundos1);
        
        int ss = diferenca % 60;
        diferenca /= 60;
        int mm = diferenca % 60;
        diferenca /= 60;
        int hh = diferenca % 24;
        
        return new Tempo(hh,mm,ss);
    }
    
    private int contarSegundos() {
        int totalSegundos;

        totalSegundos = segundos;
        totalSegundos += (minutos * 60);
        totalSegundos += ((horas * 60) * 60);

        return totalSegundos;
    }
}
