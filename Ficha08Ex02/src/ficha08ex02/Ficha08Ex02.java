/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha08ex02;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha08Ex02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Tempo t1 = new Tempo(5, 30, 12);
        
        System.out.println(t1.getHorasFormato12());
        
        t1.addSegundo();
        
        System.out.println(t1.getHorasFormato12());
        
        Tempo t2 = new Tempo(18, 5, 20);
        
        System.out.println(t2.getHorasFormato12());
        
        if (t1.isMaior(t2)) {
            System.out.println("T1 É MAIOR QUE T2!");
        } else {
            System.out.println("T1 É MENOR QUE T2!");
        }
        
        if (t1.isMaior(23, 7, 4)) {
            System.out.println("T1 É MAIOR QUE 23:07:04!");
        } else {
            System.out.println("T1 É MENOR QUE 23:07:04!");
        }
        
        System.out.println("A diferença de T1 para T2 em segundos é: " + t1.calcularDiferencaSegundos(t2) + "s");
        System.out.println("A diferença de T1 para T2 em segundos é: " + t1.calcularDiferencaTempo(t2).getHorasFormato24());
    }
    
}
