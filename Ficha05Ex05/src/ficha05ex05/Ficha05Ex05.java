/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha05ex05;

import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class Ficha05Ex05 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        final double priceHamburger = 3.00;
        final double priceCheeseBurgers = 2.50;
        final double priceBatatas = 1.50;
        final double priceRefrigerantes = 1.10;
        final double priceMilkshakes = 2.30;
        final double priceGelados = 2.50;
        
        double precoTotal = 0;
        
        System.out.println("Digite a quantidade de Hambúrgers consumidos: ");
        precoTotal = (scanner.nextInt() * priceHamburger);
        
        System.out.println("Digite a quantidade de Cheeseburgers consumidos: ");
        precoTotal = precoTotal + (scanner.nextInt() * priceCheeseBurgers);
        
        System.out.println("Digite a quantidade de Batatas Fritas consumidas: ");
        precoTotal = precoTotal + (scanner.nextInt() * priceBatatas);
        
        System.out.println("Digite a quantidade de Refrigerantes consumidos: ");
        precoTotal = precoTotal + (scanner.nextInt() * priceRefrigerantes);
        
        System.out.println("Digite a quantidade de Milkshakes consumidos: ");
        precoTotal = precoTotal + (scanner.nextInt() * priceMilkshakes);
        
        System.out.println("Digite a quantidade de Gelado consumidos: ");
        precoTotal = precoTotal + (scanner.nextInt() * priceGelados);
        
        System.out.println("A conta final é de: " + precoTotal + "€");
    }
    
}
