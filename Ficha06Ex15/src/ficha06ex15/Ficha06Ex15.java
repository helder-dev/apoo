/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex15;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex15 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int numMercadorias;
        double valorTotalMercadorias = 0.0, mediaValorMercadorias;
        
        System.out.println("Introduza o número total de mercadorias em stock: ");
        numMercadorias = scanner.nextInt();
        
        for (int i = 0; i < numMercadorias; i++) {
            System.out.print("Introduza o valor da mercadoria " + (i + 1));
            valorTotalMercadorias = valorTotalMercadorias + scanner.nextDouble();
        }
        
        mediaValorMercadorias = (valorTotalMercadorias / numMercadorias);
        
        System.out.println("O valor total das mercadorias é de: " + valorTotalMercadorias);
        System.out.println("A média do valor de cada mercadoria é de: " + mediaValorMercadorias);
    }
    
}
