/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha05ex10;

import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class Ficha05Ex10 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        final double precoRelvado = 10.00;
        final double precoGrama = 15.00;
        final double precoArvores = 10.00;
        final double precoArbustos = 10.00;
        final double precoTrabalho = 10.00;
        
        int BI, NC, arvores, arbustos, horas;
        double relvado, grama, totalIVA, total;
        
        System.out.println("Introduza os m2 de Relvado: ");
        relvado = scanner.nextDouble();
        
        System.out.println("Introduza os m2 de Grama: ");
        grama = scanner.nextDouble();
        
        System.out.println("Introduza o número de Árvores: ");
        arvores = scanner.nextInt();
        
        System.out.println("Introduza o número de Arbustos: ");
        arbustos = scanner.nextInt();
        
        System.out.println("Introduza a número de horas necessárias: ");
        horas = scanner.nextInt();
        
        System.out.println("Introduza o BI do Cliente: ");
        BI = scanner.nextInt();
        
        System.out.println("Introduza o NC do Cliente: ");
        NC = scanner.nextInt();
        
        
        totalIVA = (((relvado * precoRelvado) * 0.23) + ((grama * precoGrama) * 0.23) + ((arvores * precoArvores) * 0.23) + ((arbustos * precoArbustos) * 0.23) + ((horas * precoTrabalho) * 0.23));
        total = (((relvado * precoRelvado) * 1.23) + ((grama * precoGrama) * 1.23) + ((arvores * precoArvores) * 1.23) + ((arbustos * precoArbustos) * 1.23) + ((horas * precoTrabalho) * 1.23));
        
        System.out.println(String.format("%10s %90s", "BI:" + BI, ""));
        System.out.println(String.format("%10s %90s", "NC:" + NC, ""));
        System.out.println(String.format("%s", "-----------------------------------------------------------------------------------------------------------------------"));
        System.out.println(String.format("%20s %5s %10s %5s %10s %5s %10s %5s %10s %5s %15s", "Serviço", "|", "Qtd", "|", "Custo Uni.", "|", "Custo Total", "|", "IVA (23%)", "|", "Total"));
        System.out.println(String.format("%s", "-----------------------------------------------------------------------------------------------------------------------"));
        System.out.println(String.format("%20s %5s %10s %5s %10s %5s %10s %5s %10s %5s %15s", "Relvado", "|", relvado + " m2", "|", precoRelvado + " €/m2", "|", (relvado * precoRelvado) + " €", "|", ((relvado * precoRelvado) * 0.23) + " €", "|", ((relvado * precoRelvado) * 1.23) + " €"));
        System.out.println(String.format("%20s %5s %10s %5s %10s %5s %10s %5s %10s %5s %15s", "Grama", "|", grama + " m2", "|", grama + " €/m2", "|", (grama * precoGrama) + " €", "|", ((grama * precoGrama) * 0.23) + " €", "|", ((grama * precoGrama) * 1.23) + " €"));
        System.out.println(String.format("%20s %5s %10s %5s %10s %5s %10s %5s %10s %5s %15s", "Árvores", "|", arvores + " m2", "|", precoArvores + " €/m2", "|", (arvores * precoArvores) + " €", "|", ((arvores * precoArvores) * 0.23) + " €", "|", ((arvores * precoArvores) * 1.23) + " €"));
        System.out.println(String.format("%20s %5s %10s %5s %10s %5s %10s %5s %10s %5s %15s", "Arbustos", "|", arbustos + " m2", "|", precoArbustos + " €/m2", "|", (arbustos * precoArbustos) + " €", "|", ((arbustos * precoArbustos) * 0.23) + " €", "|", ((arbustos * precoArbustos) * 1.23) + " €"));
        System.out.println(String.format("%20s %5s %10s %5s %10s %5s %10s %5s %10s %5s %15s", "Trabalho /h", "|", horas + " m2", "|", precoTrabalho + " €/m2", "|", (horas * precoTrabalho) + " €", "|", ((horas * precoTrabalho) * 0.23) + " €", "|", ((horas * precoTrabalho) * 1.23) + " €"));
        System.out.println(String.format("%s", "-----------------------------------------------------------------------------------------------------------------------"));
        System.out.println(String.format("%20s %5s %10s %5s %10s %5s %10s %5s %10s %5s %15s", "Total", "|", "", "|", "", "|", "", "|", totalIVA + " €", "|", total + " €"));
    }
    
}
