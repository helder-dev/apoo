/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha05ex09;

import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class Ficha05Ex09 {

    private static final Scanner scanner = new Scanner(System.in);
    private static final int NUM_TESTES = 4;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int numAluno = 0;
        double[] classificacoes = new double[NUM_TESTES];
        double totalClassificacao = 0.0, mediaClassificacao, DP = 0.0;
        
        System.out.println("Digite o número do aluno: ");
        numAluno = scanner.nextInt();
        
        for(int i = 0; i < NUM_TESTES; i++) {
            
            do { 
                System.out.print("Digite a classificação do teste " + (i + 1) + ": ");
                classificacoes[i] = scanner.nextDouble();
            } while (classificacoes[i] < 0.0 || classificacoes[i] > 20.0);
            
            
            totalClassificacao = totalClassificacao + classificacoes[i] ;
        }
        
        mediaClassificacao = (totalClassificacao / NUM_TESTES);
        
        for(int i = 0; i < NUM_TESTES; i++) {
            DP = DP + (Math.pow((classificacoes[i] - mediaClassificacao), 2));
        }
        
        DP = Math.sqrt(DP / NUM_TESTES);
        
        System.out.println("A Média dos testes é de: " + mediaClassificacao);
        System.out.println("O Devio Padrão dos testes é de: " + DP);
        
        scanner.close();
    }
    
}
