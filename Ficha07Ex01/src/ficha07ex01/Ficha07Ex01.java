/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha07ex01;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha07Ex01 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Pessoa p1 = new Pessoa();
        
        System.out.println("Introduza o seu nome: ");
        p1.setNome(scanner.nextLine());
        
        System.out.println("Introduza a sua idade: ");
        p1.setIdade(scanner.nextInt());
        
        System.out.println(p1.toString());
    }
    
}
