/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha07ex01;

/**
 *
 * @author Hélder Oliveira
 */
public class Pessoa {
    private String nome;
    private int idade;
    
    /**
     * Construtor sem parâmetros.
     */
    public Pessoa() {}
    
    /**
     * Construtor com parâmetro nome.
     * @param nome 
     */
    public Pessoa(String nome) {
        this.nome = nome;
    }
    
    /**
     * Construtor com parâmetro nome e idade.
     * @param nome
     * @param idade 
     */
    public Pessoa(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
    }
    
    
    /**
     * Devolve o nome da pessoa.
     * @return 
     */
    public String getNome() {
        return nome;
    }
    
    /**
     * Devolve a idade da pessoa.
     * @return 
     */
    public int getIdade() {
        return idade;
    }

    /**
     * Modifica o nome da pessoa.
     * @param nome 
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Modifica a idade da pessoa.
     * @param idade 
     */
    public void setIdade(int idade) {
        this.idade = idade;
    }
    
    
    /**
     * Devolve uma string com a representação interna do objeto.
     * @return 
     */
    public String toString() {
        return nome + " tem " + idade + " anos.";
    }
}
