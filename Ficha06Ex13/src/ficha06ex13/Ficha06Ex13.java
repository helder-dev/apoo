/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex13;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex13 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int numero;
        
        System.out.print("Introduza o número: ");
        numero = scanner.nextInt();
        
        if (isPrime(numero)) {
            System.out.println("O NÚMERO É PRIMO!");
        } else {
            System.out.println("O NÚMERO NÃO É PRIMO!");
        }
    }
    
    private static boolean isPrime(int numero) {
        for (int i = 2; i < numero; i++) {
            if (numero % i == 0)
                return false;   
        }
        return true;
    }
}
