/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha08ex03;

/**
 *
 * @author Hélder Oliveira
 */
public class Utentes {
    private Utente[] utentes;
    private int numUtentes;
    
    private static final int MAX_UTENTES = 1000;
    
    public Utentes() {
        this.numUtentes = 0;
        this.utentes = new Utente[MAX_UTENTES];
    }
    
    
    public Utente getUtenteByNome(String nome) {
        Utente u = null;
        int i = 0;

        while ((i < MAX_UTENTES) && (!(this.utentes[i].getNome()).equals(nome))) { 
            i++; 
        }
        
        if (i < MAX_UTENTES) {
            u = this.utentes[i];
        }
        
        return u;
    }
    
    public void inserirUtente(String nome, String genero, int idade, double altura, double peso) {
        if (numUtentes < MAX_UTENTES) {
            if (getUtenteByNome(nome) == null) {
                this.utentes[this.numUtentes] = new Utente(nome, genero, idade, altura, peso);  
                this.numUtentes++;
            } else {
                System.out.println("O utente já existe.");
            }
        } else {
            System.out.println("Não é possível adicionar mais utentes.");
        }
    }
    
    
}
