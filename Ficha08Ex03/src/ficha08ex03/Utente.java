/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha08ex03;

/**
 *
 * @author Hélder Oliveira
 */
public class Utente {
    private String nome;
    private String genero;
    private int idade;
    private double altura;
    private double peso;
    
    public Utente(String nome, String genero, int idade, double altura, double peso) {
        this.nome = nome;
        this.genero = genero;
        this.idade = idade;
        this.altura = altura;
        this.peso = peso;
    }

    
    public String getNome() {
        return this.nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getGenero() {
        return this.genero;
    }
    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    public int getIdade() {
        return this.idade;
    }
    public void setIdade(int idade) {
        this.idade = idade;
    }

    public double getAltura() {
        return this.altura;
    } 
    public void setAltura(double altura) {
        this.altura = altura;
    }
    
    public double getPeso() {
        return this.peso;
    }
    public void setPeso(double peso) {
        this.peso = peso;
    }
    

    public String getAtributosUtente() {
        return "Nome: " + nome + " \nGénero: " + genero + " \nIdade: " + idade + " \nAltura: "+ altura + " \nPeso: " + peso;
    }
    
    
    public double calcularIMC() {
        return (peso / Math.pow(altura, 2));
    }
    
    public String getGrauObesidade() {
        double IMC = calcularIMC();
        
        if (IMC < 18) {
            return "MAGRO";
        } else if (IMC >= 18 && IMC <= 25) {
            return "SAUDÁVEL";
        } else {
            return "OBESO";
        }
    }
}
