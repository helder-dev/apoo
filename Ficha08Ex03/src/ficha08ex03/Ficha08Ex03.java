/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha08ex03;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha08Ex03 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Utentes uts = new Utentes();
        
        uts.inserirUtente("Hélder", "Masculino", 23, 179, 65.5);
        
        System.out.println(uts.getUtenteByNome("Hélder").getGrauObesidade());
    }
    
}
