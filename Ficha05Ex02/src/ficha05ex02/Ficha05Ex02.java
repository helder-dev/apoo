/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha05ex02;

import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class Ficha05Ex02 {
    
    private static final Scanner scanner = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int anos = 0, meses = 0, dias = 0;
        
        System.out.println("Digite o número de anos: ");
        anos = scanner.nextInt();

        System.out.println("Digite o número de meses: ");
        meses = scanner.nextInt();

        System.out.println("Digite o número de dias: ");
        dias = scanner.nextInt();

        System.out.println("A sua idade em dias é: " + ((360 * anos) + (30 * meses) + dias));
    }
    
}
