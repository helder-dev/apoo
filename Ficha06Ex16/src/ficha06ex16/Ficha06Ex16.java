/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex16;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex16 {

    private static final Scanner scanner = new Scanner(System.in);
    
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int tentativas, countTentativas = 0, max, jogadores, num, diff, jogadas = 1;
        boolean acertou = false;
        
        System.out.println("Introduza o número máximo de tentativas: ");
        tentativas = scanner.nextInt();
        
        System.out.println("Introduza o número máximo a adivinhar: ");
        max = scanner.nextInt();
        
        System.out.println("Introduza o número de jogadores: ");
        jogadores = scanner.nextInt();
        
        int randomNumber =  new Random().nextInt(max + 1);
        
        do {
            for (int i = 1; i <= jogadores; i++) {
                System.out.print("JOGADOR " + i + " ["+ jogadas +"ª Tentativa]: Introduza o seu palpite: ");
                num = scanner.nextInt();

                diff = Math.abs(num - randomNumber);

                if (diff == 0) {
                    acertou = true;
                    System.out.println(ANSI_GREEN + "ACERTOU!" + ANSI_RESET);
                    break;
                } else if (diff > 0 && diff <= 5) {
                    System.out.println(ANSI_RED + "QUENTE" + ANSI_RESET);
                } else if (diff > 5 && diff <= 25) {
                    System.out.println(ANSI_YELLOW + "MORNO" + ANSI_RESET);
                } else {
                    System.out.println(ANSI_BLUE + "FRIO" + ANSI_RESET);
                }
                
                countTentativas++;
            }
            
            jogadas++;
        } while ((acertou == false) && (countTentativas < (tentativas * jogadores)));
        
        if (acertou == false) {
            System.out.println("Esgotaram o número máximo de tentativa.");
        }
    }
}
