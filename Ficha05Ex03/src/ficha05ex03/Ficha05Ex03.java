/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha05ex03;

import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class Ficha05Ex03 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    	double c1 = 0, c2 = 0;
    
        System.out.println("Digite o valor do cateto 1: ");
    	c1 = scanner.nextInt();
    
        System.out.println("Digite o valor do cateto 2: ");
    	c2 = scanner.nextInt();
    
        System.out.println("O valor da hipotenusa é: " + Math.sqrt(Math.pow(c1, 2) + Math.pow(c2, 2)));
    }
    
}
