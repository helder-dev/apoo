/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex05;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex05 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int idade;
        
        System.out.println("Introduza a idade do nadador: ");
        idade = scanner.nextInt();
        
        if (idade >= 5 && idade <=7) {
            System.out.println("Infantil A");
        } else if (idade >= 8 && idade <=11) {
            System.out.println("Infantil B");
        } else if (idade >= 12 && idade <=13) {
            System.out.println("Juvenil A");
        } else if (idade >= 14 && idade <=17) {
            System.out.println("Juvenil B");
        } else if (idade >= 18){
            System.out.println("Maiores 18");
        }
    }
    
}
