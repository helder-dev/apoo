/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex03;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex03 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String nome;
        int dias;
        double total;
        
        System.out.println("Introduza o seu nome: ");
        nome = scanner.nextLine();
        
        System.out.println("Introduza o número de dias de alojamento: ");
        dias = scanner.nextInt();
        
        total = (60 * dias);
        
    	if (dias < 8) {
            total = total + (dias * 8);
        } else if (dias >= 8 && dias <= 15) {
            total = total + (dias * 6);
        } else {
            total = total + (dias * 5.5);
        }
        
        System.out.println("Nome: " + nome);
        System.out.println("Total: " + total + "€");
    }
    
}
