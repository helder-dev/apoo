/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha05ex08;

import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class Ficha05Ex08 {
    
    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String BI;
        int numAluno;
        double notaMT1, notaMT2, notaMT3, classificacaoMT,
                notaT1, notaT2, classificacaoT,
                aulasAssistidas, aulasDadas, classificacaoAulas,
                CAD;
        
        
        System.out.println("Digite o número do BI do aluno: ");
        BI = scanner.nextLine();
        
        System.out.println("Digite o número do aluno: ");
        numAluno = scanner.nextInt();
        
        
        System.out.println("Digite a nota do Mini-Teste 1: ");
        notaMT1 = scanner.nextDouble();
        
        System.out.println("Digite a nota do Mini-Teste 2: ");
        notaMT2 = scanner.nextDouble();
       
        System.out.println("Digite a nota do Mini-Teste 3: ");
        notaMT3 = scanner.nextDouble();
        
        classificacaoMT = (notaMT1 + notaMT2 + notaMT3) / 3;


        System.out.println("Digite a nota do Trabalho 1: ");
        notaT1 = scanner.nextDouble();
        
        System.out.println("Digite a nota do Trabalho 2: ");
        notaT2 = scanner.nextDouble();
        
        classificacaoT = (notaT1 * 0.4) + (notaT2 * 0.6);
        
        
        System.out.println("Digite o número de aulas dadas: ");
        aulasDadas = scanner.nextInt();
        
        System.out.println("Digite o número de aulas assistidas: ");
        aulasAssistidas = scanner.nextInt();
        
        classificacaoAulas = ((aulasAssistidas / aulasDadas) * 20);
        
        CAD = ((classificacaoMT * 0.5) + (classificacaoT * 0.4) + (classificacaoAulas * 0.1));
        
        System.out.println(String.format("%30s %70s", "BI:" + BI, ""));
        System.out.println(String.format("%30s %70s", "Nº Aluno:" + numAluno, ""));
          System.out.println(String.format("%s", "-----------------------------------------------------------------------------------------------------------------------"));
        System.out.println(String.format("%30s %5s %10s %5s %10s %5s %10s %5s %20s", "Itens", "|", "#1", "|", "#2", "|", "#3", "|", "Classificação"));
        System.out.println(String.format("%s", "-----------------------------------------------------------------------------------------------------------------------"));
        System.out.println(String.format("%30s %5s %10s %5s %10s %5s %10s %5s %20s", "Mini-Testes", "|", notaMT1, "|", notaMT2, "|", notaMT3, "|", classificacaoMT));
        System.out.println(String.format("%30s %5s %10s %5s %10s %5s %10s %5s %20s", "Trabalhos", "|", notaT1, "|", notaT2, "|", "", "|", classificacaoT));
        System.out.println(String.format("%30s %5s %10s %5s %10s %5s %10s %5s %20s", "Assiduidade", "|", classificacaoAulas, "|", "", "|", "", "|", classificacaoAulas));
        System.out.println(String.format("%30s %5s %10s %5s %10s %5s %10s %5s %20s", "Classificação AD", "|", "", "|", "", "|", "", "|", CAD));
        
        scanner.close();  
    }
    
}
