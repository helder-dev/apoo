/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex09;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex09 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double massa;
        int count = 0;
        
        System.out.println("Introduza a massa do material radioactivo: ");
        massa = scanner.nextDouble();
        
        while (massa >= 0.5) {
            massa = (massa / 2);
            count++;
        }
        
        System.out.println("A massa do material radioactivo será inferior a 0.5 passados " + (count * 50) + " segundos.");
    }
    
}
