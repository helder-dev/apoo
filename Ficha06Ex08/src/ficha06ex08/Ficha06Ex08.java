/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex08;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex08 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double N, valor, A = 0.0;
        
        do {
             System.out.println("Digite a quantidade de números que quer introduzir:");
             N = scanner.nextInt();
        } while (N <= 0);
        
        for(int i = 0; i < N; i++) {
            System.out.println("Introduza o " + (i + 1) + "º valor:");
            valor = scanner.nextInt();
            
            A = A + ((valor - i) / (i + 1));
        }
        
        System.out.println("A = " + A);
    }
    
}
