/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex06;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex06 {

    private static final Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double indicePoluicao;
        
        System.out.println("Introduza o índice de poluição medido: ");
        indicePoluicao = scanner.nextDouble();
        
        if (indicePoluicao >= 0.3 && indicePoluicao < 0.4) {
            System.out.println("As indústrias do 1º grupo são intimadas a suspenderem suas atividades.");
        } else if (indicePoluicao >= 0.4 && indicePoluicao < 0.5)  {
            System.out.println("As indústrias do 1º e 2º grupo são intimadas a suspenderem suas atividades.");
        } else if (indicePoluicao >= 0.5) {
            System.out.println("As indústrias do 1º, 2º e 3º grupo devem de suspender suas atividades.");
        }
    }
    
}
