/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha07ex02;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha07Ex02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Data data1 = new Data(2018, 11, 29);
        System.out.println(data1.toString());
        
        Data data2 = new Data(1974, 4, 25);
        System.out.println(data2.toAnoMesDiaString());
        
        if (data1.isMaior(data2)) {
            System.out.println("De facto data1 é maior que data2");
        }
        
        System.out.println("Nº Dias entre data1 e data2: " + data1.calcularDiferenca(data2));
        
        System.out.println("Nº Dias para o Natal: " + data1.calcularDiferenca(2018,12,25));
        
        System.out.println("Dia da semana em que ocorreu o 25 de Abril: " + data2.determinarDiaDaSemana());
        
        if (data2.isAnoBissexto(data2.getAno())) {
            System.out.println("É BISSEXTO!");
        } else {
            System.out.println("NÃO É BISSEXTO!");
        }
        
        if (Data.isAnoBissexto(data2.getAno())) {
            System.out.println("É BISSEXTO!");
        } else {
            System.out.println("NÃO É BISSEXTO!");
        } 
    }
}
