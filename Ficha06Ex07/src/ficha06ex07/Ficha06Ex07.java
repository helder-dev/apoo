/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06ex07;

import java.util.Scanner;

/**
 *
 * @author Hélder Oliveira
 */
public class Ficha06Ex07 {
    
    private static final Scanner scanner = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double N, countPositivos = 0, countNegativos = 0, valor, mediaNegativos = 0.0, percPositivos;
        
        do {
             System.out.println("Digite a quantidade de números que quer introduzir:");
             N = scanner.nextInt();
        } while (N <= 0);
        
        for(int i = 0; i < N; i++) {
            System.out.println("Introduza o " + (i + 1) + "º valor:");
            valor = scanner.nextDouble();
            
            if (valor > 0) {
                countPositivos++;
            } else if (valor < 0) {
                mediaNegativos = mediaNegativos + valor;
                countNegativos++;
            }
        }
        
        percPositivos = ((countPositivos / N) * 100);
        mediaNegativos = (mediaNegativos / countNegativos);

        System.out.println("Percentagem números positivos: " + percPositivos + "%");
        System.out.println("Média de números negativos: " + mediaNegativos);
    }
    
}
